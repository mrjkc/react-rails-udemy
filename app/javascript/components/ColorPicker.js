import React from "react"
import PropTypes from "prop-types"

import { SketchPicker } from "react-color" //we added this with yarn add component

class ColorPicker extends React.Component {

  constructor(props) {
      super(props);

      this.state = { selector: props.selector };

      this.handleChange = this.handleChange.bind(this);
  }

  handleChange = (color) => {
    document.body.style.background = color.hex;
    let elem = document.querySelector('#' + this.state.selector);

    elem.value = color.hex
  }

  render() {
    return <React.Fragment>
             <SketchPicker color={this.props.color}
                           onChange={this.handleChange} /> //on change is which function is going to handle a change in our sketchpicker
           </React.Fragment>
  }
}

ColorPicker.propTypesypes = {};

export default ColorPicker;
